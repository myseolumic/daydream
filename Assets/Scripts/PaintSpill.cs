﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintSpill : MonoBehaviour
{
    private float timer = 0;

    void Update()
    {
        if (timer > 5)
        {
            Destroy(this.gameObject);
        }

        timer += Time.deltaTime;
    }
}
