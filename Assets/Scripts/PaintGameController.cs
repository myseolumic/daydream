﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintGameController : MonoBehaviour
{

    public float FillAmmount = -1f;
    private float MaxTime = 10f;
    private float ActiveTime = 0f;
    public ColorCircle[] Circles;
    private float percent = 0;

    public PlayerController playerController;

    public Transform characterSpawn;
    public Transform roomCenter;
    public GameObject dadRoomDoor;

    void Start()
    {
        print(Circles.Length);
    }

    void Update()
    {
        if(percent < 1 && percent >= 0)
        {
            
            float CorrectAmmount = GetCorrectAmmount();
            // see if else mby ebavajalik, ma liiga väsinud.
            if(UI.instance.PaintingProgress.fillAmount == 0 && ActiveTime <= 0)
            {

                ActiveTime = 0;
                ActiveTime += Time.deltaTime * (FillAmmount + CorrectAmmount * 0.5f);
            }
            else
            {
                ActiveTime += Time.deltaTime * (FillAmmount + CorrectAmmount * 0.5f);
            }
            
            print("Active time: " + ActiveTime);
            percent = Mathf.Lerp(0, 1,ActiveTime / MaxTime);
            print(percent);
            UI.instance.PaintingProgress.fillAmount = Mathf.Lerp(0, 1, percent);
               
        }
        if(percent == 1)
        {
            UI.instance.PaintingProgress.fillAmount = 1;
            playerController.characterSpawn = characterSpawn;
            playerController.roomCenter = roomCenter;
            playerController.TeleportType = 0;
            playerController.Blink.SetTrigger("Active");
            dadRoomDoor.SetActive(true);
        }
        
    }

    private float GetCorrectAmmount()
    {
        float counter = 0;
        
        for (int i = 0; i < Circles.Length; i++)
        {
            if (Circles[i].IsCorrect) counter++;
        }
        print(counter);
        return counter;
    }
}
