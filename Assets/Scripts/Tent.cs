﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tent : MonoBehaviour, InteractableItem {

    public ToySpawn toySpawn;
    private PlayerController playerController;
    public Transform characterSpawn;
    public Transform roomCenter;

    public Trap trapPrefab;

    private void Start()
    {
        playerController = PlayerController.player;
    }

    public void Interact()
    {
        PlayerController.player.trapPrefab = this.trapPrefab;
        playerController.fightingTeddy = true;

        UI.instance.ChangeInfoBoxStatus(true);
        //UI.instance.showText("Oh no!... There is a soldier charging at your direction. Try to push him out of the door. Use SPACEBAR to push them.", 0.05f);
        playerController.characterSpawn = characterSpawn;
        playerController.roomCenter = roomCenter;
        playerController.Blink.SetTrigger("Active");
        playerController.TeleportType = 0;

        toySpawn.setCurrentWave(playerController.currentToyWave);
        toySpawn.StartWave();


    }
}
