﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorCircle : MonoBehaviour, InteractableItem
{
    public Color[] ColorsInRotation = { Color.red, Color.blue, Color.green, Color.yellow };
    public Color[] PlayerColors = { Color.red, Color.blue, Color.green, Color.yellow };
    public Color PlayerColor;
    public Color RequiredColor;
    public SpriteRenderer PlayerHalf;
    public SpriteRenderer RequiredHalf;
    public int Index = 0;
    public int LoopIndex = 0;
    public bool IsCorrect;

    public AudioClipGroup clickCircle;

    void Start()
    {
        Shuffle(ColorsInRotation);
        InvokeRepeating("ChangeColor", 0, 10);
    }

    void Update()
    {
        if(PlayerColor == RequiredColor)
        {
            IsCorrect = true;
        }
        else
        {
            IsCorrect = false;
        }
    }

    public void Interact()
    {
        clickCircle.Play();
        PlayerColor = PlayerColors[Index];
        PlayerHalf.color = PlayerColor;
        if (Index == PlayerColors.Length - 1)
        {
            Index = 0;
        }
        else
        {
            Index++;
        }

    }

    void ChangeColor()
    {
        Shuffle(ColorsInRotation);
        RequiredColor = ColorsInRotation[LoopIndex];
        RequiredHalf.color = RequiredColor;
        if (LoopIndex == ColorsInRotation.Length - 1)
        {
            LoopIndex = 0;
        }
        else
        {
            LoopIndex++;
        }
    }

    void Shuffle(Color[] colors)
    {
        for (int t = 0; t < colors.Length; t++)
        {
            Color tmp = colors[t];
            int r = Random.Range(t, colors.Length);
            colors[t] = colors[r];
            colors[r] = tmp;
        }
    }
}
