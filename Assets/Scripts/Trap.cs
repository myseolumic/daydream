﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    private float timer = 0;
    public Animator animator;
    public AudioClipGroup soundEffect;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (timer > 3)
        {
            Activate();
        }

        if (timer > 13)
        {
            Trigger();
        }

        timer += Time.deltaTime;
    }

    void Activate()
    {

        GetComponent<CircleCollider2D>().enabled = true;
        GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        //Add activation animation
    }

    public void Trigger()
    {
        soundEffect.Play();
        animator.SetTrigger("Close");
    }

    void Delete()
    {
        Destroy(this.gameObject);
    }
}
