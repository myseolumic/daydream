﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cupboard : MonoBehaviour, InteractableItem {
    
    public PushingEnemy teddy;
   // public PushingEnemyDO enemyData;
    public AudioClipGroup teddyFightTrack;
    public AudioClipGroup teddySlamSound;

    private PlayerController playerController;
    public Transform characterSpawn;
    public Transform roomCenter;
    public Trap trapPrefab;

    private void Start()
    {
        playerController = PlayerController.player;
    }
    public void Interact()
    {
        playerController.characterSpawn = characterSpawn;
        playerController.roomCenter = roomCenter;
        playerController.Blink.SetTrigger("Active");
        playerController.TeleportType = 0;
        playerController.fightingTeddy = true;
        playerController.trapPrefab = this.trapPrefab;
        

        PushingEnemy teddyInstance = Instantiate(teddy, new Vector2(-4f, -10f), Quaternion.identity);
      //  teddyInstance.slamPower = enemyData.strength;
      //  teddyInstance.speed = enemyData.speed;
      //  teddyInstance.sprite = enemyData.sprite;
        teddyInstance.soundTrack = teddyFightTrack;
        teddyInstance.attackSound = teddySlamSound;
        UI.instance.showText("You find a purple teddy bear in the top drawer." +
            " That’s odd, you have never seen it in your home before....." +
            " As you lift the toy, you daydream… ", 0.03f);
    }
}
