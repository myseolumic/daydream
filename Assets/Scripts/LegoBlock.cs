﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegoBlock : MonoBehaviour {

    public string blockID;

    private bool attached;
    private bool dropping;
    private float time;
    private int directionCounter;

    private Vector2 direction;
    private Rigidbody2D rb;

    private void Start()
    {
        attached = false;
        dropping = false;

        time = 0;
        direction = Vector2.left;
        directionCounter = 3;

        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (dropping)
        {
            transform.Translate(Vector2.down*Time.deltaTime*3);
            //rb.velocity = Vector2.down * 3;
        }

        if (!attached && !dropping)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                dropping = true;
                return;
            }

            time += Time.deltaTime;
            
            if(time >= 1)
            {
                transform.Translate(direction);
                time -= 1;
                directionCounter--;
                if(directionCounter == 0)
                {
                    if (direction == Vector2.left)
                        direction = Vector2.right;
                    else
                        direction = Vector2.left;
                    directionCounter = 7;
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Block")
        {
            attached = true;
            dropping = false;
            Vector2 pos = transform.position;

            //lisasin selle rea:
            rb.bodyType = RigidbodyType2D.Dynamic;

            if(collision.transform.position.y-transform.position.y < LegoBlockDropper.instance.counter)
            {
                //lost minigame
            }

            //eemaldasin selle rea:
            //pos.y = collision.transform.position.y + 1;

            transform.position = pos;
            LegoBlockDropper.instance.droppedLast = true;
        }
    }

}
