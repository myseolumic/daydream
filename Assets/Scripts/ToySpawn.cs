﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToySpawn : MonoBehaviour
{
    public PlayerController playerController;
    public ToySoldier toySoldier;
    public PushingEnemyDO enemyData;
    public List<int> Wave1 = new List<int> {1,2,2,3};
    public List<int> Wave2 = new List<int> {3,4,5};
    private List<int> chosenWave = null;

    private int waveIndex = 0;
    private bool wavebool = true;

    public PickUppableItem keyPrefab;

    // Use this for initialization
    public void StartWave()
    {
        StartCoroutine(NextWave(chosenWave));
    }

    private void Update()
    {

    }

    public void setCurrentWave(int waveNumber)
    {
        if (waveNumber == 1)
        {
            chosenWave = Wave1;
        } else if (waveNumber == 2)
        {
            chosenWave = Wave2;
        }
    }


    public IEnumerator NextWave(List<int> Wave)
    {
        if (chosenWave != null)
        {
            while (wavebool)
            {

                if (GameObject.FindWithTag("ToySoldier") == null)
                {
                    if (waveIndex == Wave.Count)
                    {

                        UI.instance.CompleteDungeon();
                        wavebool = false;
                        Wave = null;
                        Instantiate(keyPrefab, transform.position, Quaternion.identity);
                        
                    }

                    if (Wave != null)
                    {
                        for (int i = 0; i < Wave[waveIndex]; i++)
                        {
                            Instantiate(toySoldier, transform.position, Quaternion.identity);
                            //toy.slamPower = enemyData.strength;
                            //toy.speed = enemyData.speed;
                           // toy.sprite = enemyData.sprite;


                            yield return new WaitForSeconds(0.5f);
                        }
                        waveIndex++;
                    }
                    

                }
                yield return null;


            }
        }

    }

}


