﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class comboDoor : MonoBehaviour {

    public GameObject pad1;
    public GameObject pad2;
    public GameObject pad3;

    public static string combination;


    // Use this for initialization
    void Start () {
        combination = "";
	}
	
	// Update is called once per frame
	void Update () {
		if(combination == "r")
        {
            print(combination);
            if(combination == "rg")
            {
                print(combination);
                if (combination == "rgb")
                {
                    print(combination);
                    this.gameObject.SetActive(false);
                }
            }
        }

        ResetDoor();
	}

    void ResetDoor()
    {
        combination = "";
        pad1.gameObject.SetActive(true);
        pad2.gameObject.SetActive(true);
        pad3.gameObject.SetActive(true);
    }
}
