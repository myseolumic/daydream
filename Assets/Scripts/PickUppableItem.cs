﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUppableItem : MonoBehaviour {

    public PickUppableItem instance;
    public Sprite image;
    public string type;

	// Use this for initialization
	void Start () {
		if(type == null)
        {
            type = "white";
        }
        instance = this;
	}
}
