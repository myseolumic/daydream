﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public enum Direction {
    Down = 0x01,
    Up = 0x02,
    Left = 0x03,
    Right = 0x04,
}

public class Door : MonoBehaviour {

    

    public GameObject characterSpawn;
    public AudioClipGroup doorSound;

    public Direction direction;

    public string textToPlay = "";

    public string keyType;


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            doorSound.Play();
            Debug.Log("Need key: " + keyType + ", got key: " + UI.instance.checkInventoryForItem(keyType));
            if (keyType != "" && !UI.instance.checkInventoryForItem(keyType))
            {
                return;
            }

            //todo: Make ui do something to smoothe out the transition
            Vector3 cameraNextPosition = Camera.main.transform.position;
            cameraNextPosition.z = -10;

            if (textToPlay != "")
            {
                UI.instance.showText(textToPlay, 0.05f);
            }

            if (direction == Direction.Down)
            {
                cameraNextPosition.y += -10;
            }
            if (direction == Direction.Up)
            {
                cameraNextPosition.y += 10;
            }
            if (direction == Direction.Left)
            {
                cameraNextPosition.x += -18;
            }
            if (direction == Direction.Right)
            {
                cameraNextPosition.x += 18;
            }
            Camera.main.transform.position = cameraNextPosition;
            PlayerController.player.transform.position = characterSpawn.transform.position;
        }

    }
}
