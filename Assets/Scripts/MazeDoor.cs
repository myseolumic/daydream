﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeDoor : MonoBehaviour
{
    public bool isCorrect = false;
    public BoxCollider2D doorCollider;

    public void TryDoor()
    {
        if (isCorrect)
        {
            doorCollider.enabled = false;
        }
        else
        {
            print("wrong choice");
            PlayerController.player.characterSpawn = PlayerController.player.mazeWrongChoiceSpawn;
            PlayerController.player.Teleport();
        }
    }

    void Update()
    {
        
    }
}
