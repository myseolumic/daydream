﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {

    public GameObject projectilePrefab;
    public float shotDelay = 1;
    public bool readyToShoot = false;
    private float shotTime;

    public AudioClipGroup shootEffect;

    void Update () {
        if (readyToShoot)
        {
            if(Time.time > shotTime)
            {
                Shoot();
                shotTime = Time.time + shotDelay;
            }
        }
    }

    void Shoot()
    {
        if (!readyToShoot) return;
        shootEffect.Play();
        Instantiate(projectilePrefab, transform.position, transform.rotation);
    }
}
