﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToySoldier : MonoBehaviour
{
    public float speed = 2;
    public float health = 3;
    public AudioClipGroup soundTrack;
    public AudioClipGroup attackSound;

    private Transform target;
    private Rigidbody2D TeddyRb;
    private Rigidbody2D PlayerRb;

    //New fighting logic
    public float slamRange;
    public LayerMask whatIsEntities;
    public int slamPower = 1;
    public bool isSlamming = false;

    public Animator animator;

    void Start()
    {

        TeddyRb = GetComponent<Rigidbody2D>();
        PlayerRb = PlayerController.player.GetComponent<Rigidbody2D>();
        target = PlayerController.player.transform;
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    private void Update()
    {
        if (health <= 0) Destroy(this.gameObject);

        //print(isSlamming);
        if (PlayerController.player.defeated)
        {
            return;
        }
        //Enemy follob mängijat
        if (Vector2.Distance(transform.position, target.position) < 2f)
        {

            if (!isSlamming) GetComponent<Animator>().SetTrigger("Slamm");


        }
        else if (!isSlamming)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

            animator.SetFloat("Horizontal", transform.position.x);
            animator.SetFloat("Vertical", transform.position.y);
            animator.SetFloat("Magnitude", speed);

            PlaySoundTrack();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Trap")
        {
            if (health > 0) health -= 1;
            collision.GetComponent<Trap>().Trigger();
        }
    }

    void Slam()
    {
        isSlamming = true;
        attackSound.Play();
        int slamForce = 7;
        Collider2D[] entitiesToDamage = Physics2D.OverlapCircleAll(transform.position, slamRange, whatIsEntities);
        for (int i = 0; i < entitiesToDamage.Length; i++)
        {

            if (entitiesToDamage[i].GetComponent<PlayerController>())
            {
                Vector2 direction = -(transform.position - PlayerController.player.transform.position).normalized * slamForce;
                PlayerController.player.Damage(slamPower, direction);
            }

        }
        isSlamming = false;
    }

    void PlaySoundTrack()
    {
        if (soundTrack != null) soundTrack.Play();

    }
}
