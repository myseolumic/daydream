﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toilet : MonoBehaviour, InteractableItem {

    private ShootingEnemy monster;
    public GameObject Bath;
    public AudioClipGroup flush;


    void Update ()
    {
        monster = FindObjectOfType<ShootingEnemy>();
        if (monster == null)
        {
            Bath.SetActive(true);
        }
        else
        {
            Bath.SetActive(false);
        }
        
    }

    public void Interact()
    {
        // dmg monster
        flush.Play();
        monster = FindObjectOfType<ShootingEnemy>();
        if (monster != null)
        {
            monster.DealDamage();
        }
    }
}
