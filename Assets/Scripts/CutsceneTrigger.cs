﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneTrigger : MonoBehaviour {

    public Cutscene targetScene;
    public int sceneId;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && CutsceneManager.instance.cutsceneCounter == sceneId)
        {
            CutsceneManager.instance.PlayCutscene(targetScene);
        }
    }
}
