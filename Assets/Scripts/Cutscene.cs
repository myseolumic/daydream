﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Cutscene")]
public class Cutscene : ScriptableObject {

    [TextArea(3,6)]
    public string[] sentences;
    public Vector2[] playerMovementLocations;   //the point where the player has to move

    public GameObject secondaryCharacter;       //daddy goes here if we need to start implementing him
	
}
