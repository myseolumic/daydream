﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public Button startButton;
    public Button optionsButton;
    public Button quitButton;

    public Image creditsPanel;

	// Use this for initialization
	void Start () {
        startButton.onClick.AddListener(() => StartButtonPressed());
        optionsButton.onClick.AddListener(() => OptionsButtonPressed());
        quitButton.onClick.AddListener(() => QuitButtonPressed());
    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.Escape) 
            || Input.GetKeyDown(KeyCode.Space) 
            || Input.GetKeyDown(KeyCode.Return))
        {
            creditsPanel.gameObject.SetActive(false);
        }
	}

    void QuitButtonPressed()
    {
        Application.Quit();
    }

    void OptionsButtonPressed()
    {
        creditsPanel.gameObject.SetActive(true);
    }

    void StartButtonPressed()
    {
        SceneManager.LoadScene("GameScene");
    }
}
