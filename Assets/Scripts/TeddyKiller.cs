﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeddyKiller : MonoBehaviour {

    public GameObject dungeonDoorWay;
    public GameObject bathroomDoor;
    public GameObject PurpleKeyPrefab;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PushingEnemy")
        {

            UI.instance.CompleteDungeon();
            Destroy(collision.gameObject);
            dungeonDoorWay.SetActive(true);
            //replace when keys implemented
            bathroomDoor.SetActive(true);
            PlayerController.player.currentToyWave = 2;
            GameObject key = Instantiate(PurpleKeyPrefab, new Vector2(0, -10), Quaternion.identity);
        }
    }


}
