﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegoBlockDropper : MonoBehaviour {

    public int counter;
    public bool droppedLast;
    private Queue<int> droppingQueue;

    public GameObject LegoOnePrefab;
    public GameObject LegoTwoPrefab;
    public GameObject LegoThreePrefab;
    public GameObject LegoFourPrefab;

    [TextArea(5,10)]
    public string queueString;

    public static LegoBlockDropper instance;

	void Start () {
        instance = this;
        droppingQueue = new Queue<int>();
        counter = 0;
        droppedLast = true;
        FillQueue();
	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J) && droppedLast)
        {
            //PlayerController.player.SetInputEnabled(false);
            SpawnNextBlock();
        }
    }

    void FillQueue()
    {
        string[] blocks = queueString.Replace('\n', ',').Split(',');
        for(int i = blocks.Length-1; i >= 0; i--)
        {
            if (blocks[i] == ".") droppingQueue.Enqueue(1);
            else if (blocks[i] == "..") droppingQueue.Enqueue(2);
            else if (blocks[i] == "...") droppingQueue.Enqueue(3);
            else droppingQueue.Enqueue(4); // is (. .)
        }
    }

    void SpawnNextBlock()
    {
        if(droppingQueue.Count == 0)
        {
            //PlayerController.player.SetInputEnabled(true);
            return;
        }
        int id = droppingQueue.Dequeue();
        Vector3 pos = transform.position;
        pos.y += 8;

        if (id == 3) Instantiate(LegoThreePrefab, pos, Quaternion.identity);
        else if (id == 2) Instantiate(LegoTwoPrefab, pos, Quaternion.identity);
        else if (id == 1) Instantiate(LegoOnePrefab, pos, Quaternion.identity);
        else Instantiate(LegoFourPrefab, pos, Quaternion.identity);
        counter++;

        droppedLast = false;
    }
}
