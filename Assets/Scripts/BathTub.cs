﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BathTub : MonoBehaviour, InteractableItem
{
    public PlayerController playerController;
    public Transform characterSpawn;
    public Transform roomCenter;
    public PickUppableItem key;
    public GameObject MattsTable;

    public void Interact()
    {
        //UI.instance.AddToInventory(key);
        UI.instance.showText("You look into the bathtub and " +
            "find a paintbrush with some green paint on it!" +
            " “Hey, that’s just the one I needed! I should go " +
            "finish my painting!”", 0.02f);
        MattsTable.GetComponent<BoxCollider2D>().enabled = true;
        playerController.characterSpawn = characterSpawn;
        playerController.roomCenter = roomCenter;
        playerController.TeleportType = 0;
        playerController.Blink.SetTrigger("Active");

    }
}
