﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerController))]
public class SpawnPicker : Editor
{


    string nimi = "Enter spawn name here";

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        PlayerController mytarget = (PlayerController)target;
        GUILayout.Space(5);
        nimi = GUILayout.TextField(nimi, 25);
        if (GUILayout.Button("Generate new spawn prefab")) createSpawn(mytarget, nimi);

    }

    private void createSpawn(PlayerController target, string name)
    {
        GameObject spawn = new GameObject(nimi);
        spawn.transform.position = target.transform.position;
        string localPath = "Assets/Prefabs/" + spawn.name + ".prefab";
        PrefabUtility.CreatePrefab(localPath, spawn);
        
        Debug.Log("New spawn location created!");
    }

}
