﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Door))]
public class DoorEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Door mytarget = (Door) target;
        if(GUILayout.Button("Show Target CharacterSpawn")) selectSpawn(mytarget);
       
    }

    private void selectSpawn(Door target)
    {
        EditorGUIUtility.PingObject(target.characterSpawn.gameObject);
    }

}
