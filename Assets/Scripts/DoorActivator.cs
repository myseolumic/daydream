﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorActivator : MonoBehaviour
{
    public GameObject door;

    void Update()
    {
        if (FindObjectOfType<PushingEnemy>() == null)
        {
            door.SetActive(true);
        }
        else
        {
            door.SetActive(false);
        }
    }
}
