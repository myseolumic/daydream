﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour {

    public AudioClipGroup winningSound;

    public static UI instance;
    public Text winText;
    public Text loseText;
    public GameObject infoPanel;
    public Text textField;
    public Button gameButton;
    public Button statusButton;
    public Button menuButton;
    public GameObject inventoryPanel;
    public GameObject menuPanel;
    public Image PaintingProgress;
    private bool infoToggle = true;
    public bool quitting = false;
    private string lastText;
    public bool textPlaying;

    private List<GameObject> inventorySlotRefs;
    private List<PickUppableItem> items;
    private List<string> textToPlay = new List<string>();

    private void Awake()
    {
        instance = this;
    }

    void Start () {
        
        ChangeInfoBoxStatus(true);
        gameButton.onClick.AddListener(() => GameButtonClicked());
        statusButton.onClick.AddListener(() => StatusButtonClicked());
        menuButton.onClick.AddListener(() => MenuButtonClicked());

        Transform[] children = inventoryPanel.GetComponentsInChildren<Transform>();
        inventorySlotRefs = new List<GameObject>();
        items = new List<PickUppableItem>();

        foreach(Transform child in children)
        {
            if(child != children[0])
            {
                inventorySlotRefs.Add(child.gameObject);
            }
        }
        
        //showText("Matt wakes up and discovers that his marionette Bruno is missing.", 0.03f);
        //QueueText("Go check into your tent. Maybe toy soldiers saw something?");

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            toggleInfoPanel();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!textPlaying)
            {
                if(textField.text == "")
                {
                    if (textToPlay.Count > 0)
                    {
                        showText(0.03f);
                    }
                }
                else
                {
                    textField.text = "";
                }
                
               
            }
        }
    }


    public void QueueText(string text)
    {
        textToPlay.Add(text);
    }


    public void LoseGame()
    {
        loseText.text = "Dungeon Failed!";
        loseText.gameObject.SetActive(true);
    }

    void TeleportPlayer()
    {
        PlayerController.player.Teleport();
    }

    public void CompleteDungeon()
    {
        winningSound.Play();
        winText.text = "Dungeon Completed!";
        winText.gameObject.SetActive(true);
    }

    public void ChangeInfoBoxStatus(bool state)
    {       
        infoPanel.SetActive(state);
        infoToggle = !state;
    }

    public void toggleInfoPanel()
    {
        infoPanel.gameObject.SetActive(infoToggle);
        infoToggle = !infoToggle;
    }

    public void GameButtonClicked()
    {
        menuPanel.SetActive(false);
        inventoryPanel.SetActive(false);
        textField.gameObject.SetActive(true);
        showText(lastText,0.02f);
    }


    /**
     * Plays text given by text parameter.
     */
    public void showText(string text, float timePerLetter)
    {
        StopCoroutine("AnimateText");
        textPlaying = true;
        textField.text = "";
        StartCoroutine(AnimateText(text, timePerLetter));
       
    }


    /**
     * Takes the next text form textToPlay list and plays it.
     */
    public void showText(float timePerLetter)
    {
        string text = textToPlay[0];
        textToPlay.RemoveAt(0);
        StopCoroutine("AnimateText");
        textPlaying = true;
        textField.text = "";
        StartCoroutine(AnimateText(text, timePerLetter));

    }



    public void StatusButtonClicked()
    {
        menuPanel.SetActive(false);
        inventoryPanel.SetActive(true);
        textField.gameObject.SetActive(false);
        textField.text = "";

        
    }

    public void AddToInventory(PickUppableItem item)
    {
        for(int i = 0; i < 3; i++)
        {
            Color color = inventorySlotRefs[i].GetComponent<Image>().color;
            if (color.a == 0)
            {
                inventorySlotRefs[i].GetComponent<Image>().sprite = item.instance.image;
                color.a = 255;
                inventorySlotRefs[i].GetComponent<Image>().color = color;
                break;
            }
        }
        //handle full inventory
    }

    public bool checkInventoryForItem(string itemName)
    {
        for (int i = 0; i < 3; i++)
        {
            Color color = inventorySlotRefs[i].GetComponent<Image>().color;
            if (color.a == 255)
            {
                if(inventorySlotRefs[i].GetComponent<Image>().sprite.name == itemName)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void MenuButtonClicked()
    {
        //temp
        SceneManager.LoadScene("Menu");


        menuPanel.SetActive(true);
        inventoryPanel.SetActive(false);
        textField.gameObject.SetActive(false);
        textField.text = "";
    }

    IEnumerator AnimateText(string fullText, float delay)
    {
        gameButton.interactable = false;
        statusButton.interactable = false;
        menuButton.interactable = false;
        lastText = fullText;
        int i = 0;
        string currentText = "";
        while (i < fullText.Length)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                currentText = lastText;
                textField.text = currentText;
                break;
            }
            currentText += fullText[i++];
            textField.text = currentText;
            yield return new WaitForSeconds(delay);
        }

        textPlaying = false;
        gameButton.interactable = true;
        statusButton.interactable = true;
        menuButton.interactable = true;

    }

    void OnApplicationQuit()
    {
        quitting = true;
    }
}
