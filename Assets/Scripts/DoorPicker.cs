﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorPicker : MonoBehaviour
{

    public MazeDoor leftDoor;
    public MazeDoor topDoor;
    public MazeDoor rightDoor;
    public MazeDoor bottomDoor;
    public bool inTopRow;
    public bool inBottomRow;
    public List<MazeDoor> doors = new List<MazeDoor>();

    private MazeController controller;

    // Start is called before the first frame update
    void Start()
    {
        controller = FindObjectOfType<MazeController>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void ResetDoors()
    {
        leftDoor.isCorrect = false;
        topDoor.isCorrect = false;
        rightDoor.isCorrect = false;
        bottomDoor.isCorrect = false;
    }

    public void PickNextDoor()
    {
        MazeDoor chosenDoor = null;

        //if in top row pick LEFT or BOTTOM
        if (inTopRow)
        {
            doors.Add(leftDoor);
            doors.Add(bottomDoor);
            
        }
        //if in bottom row pick LEFT or TOP
        else if (inBottomRow)
        {
            doors.Add(leftDoor);
            doors.Add(topDoor);
        }
        //else pick LEFT or BOTTOM or TOP
        else
        {
            doors.Add(leftDoor);
            doors.Add(topDoor);
            doors.Add(bottomDoor);
        }

        while (chosenDoor == null)
        {
            chosenDoor = doors[Random.Range(0, doors.Count)];
            doors.Remove(chosenDoor);
            if (chosenDoor.isCorrect == true)
            {
                chosenDoor = null;
            }
        }

        chosenDoor.isCorrect = true;
    }
}
