﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float speed = 10;

    void Start()
    {

    }
    
    void Update()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }


    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        print("sein");
        if (collision.gameObject.tag == "Walls")
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        print("sein");
        if(collision.tag == "Walls")
        {
            Destroy(this.gameObject);
        }
    }
}
