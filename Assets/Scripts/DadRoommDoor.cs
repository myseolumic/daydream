﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DadRoommDoor : MonoBehaviour, InteractableItem
{
    public PlayerController playerController;
    public Transform characterSpawn;
    public Transform roomCenter;
    public bool inside;
    public MazeController maze;

    // Start is called before the first frame update
    public void Interact()
    {
        playerController.characterSpawn = characterSpawn;
        playerController.roomCenter = roomCenter;
        playerController.TeleportType = 0;
        playerController.Teleport();
        if (inside)
        {
            playerController.mazeCollider.enabled = false;
            playerController.normalCollider.enabled = true;
            playerController.speed += 3;
        }
        else
        {
            playerController.mazeCollider.enabled = true;
            playerController.normalCollider.enabled = false;
            playerController.speed -= 3;
            maze.ResetMaze();
        }
    }
}
