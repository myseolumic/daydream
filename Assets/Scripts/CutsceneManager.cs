﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneManager : MonoBehaviour {

    public static CutsceneManager instance;

    public int cutsceneCounter;

	// Use this for initialization
	void Start () {
        instance = this;
        cutsceneCounter = 0;
	}

    public void PlayCutscene(Cutscene scene)
    {
        //Disable player input;
        PlayerController.player.SetInputEnabled(false);
        PlayerController.player.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        PlayerController.player.AnimateCurrentMovement();

        //Play Cutscene
        string[] sentences = scene.sentences;
        Vector2[] points = scene.playerMovementLocations;
        StartCoroutine(DoParts(sentences, points));

    }

    IEnumerator DoParts(string[] sentences,Vector2[] points)
    {
        for (int i = 0; i < sentences.Length; i++)
        {
            string s = sentences[i];

            UI.instance.showText(s, 0.02f);

            while (UI.instance.textPlaying)
            {
                yield return null;
            }

            while (!Input.GetKeyDown(KeyCode.Space))
            {
                yield return null;
            }

            Vector3 point3 = new Vector3(points[i].x, points[i].y, 0);

            while (Vector3.Distance(point3, PlayerController.player.GetComponent<Transform>().position) > 1.5f)
            {
                Vector3 playerPos = PlayerController.player.GetComponent<Transform>().position;
                Vector2 between = point3 - playerPos;  //gets me distance in x and distance in y

                Vector3 movementVector = between;  //wonky as hell but works.

                PlayerController.player.GetComponent<Rigidbody2D>().velocity = movementVector;
                PlayerController.player.AnimateCurrentMovement();
                yield return null;
            }
            PlayerController.player.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            PlayerController.player.AnimateCurrentMovement();

        }

        //Enable player input
        PlayerController.player.SetInputEnabled(true);
    }
}
