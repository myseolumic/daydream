﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegoCorner : MonoBehaviour, InteractableItem
{
    public PlayerController playerController;

    public Transform characterSpawn;
    public Transform roomCenter;

    public void Interact()
    {
        playerController.characterSpawn = characterSpawn;
        playerController.roomCenter = roomCenter;
        playerController.TeleportType = 0;
        playerController.Blink.SetTrigger("Active");

    }
}
