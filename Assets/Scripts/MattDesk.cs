﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MattDesk : MonoBehaviour, InteractableItem
{
    public PlayerController playerController;
    public Transform characterSpawn;
    public Transform roomCenter;
    public GameObject colorGame;

    public void Interact()
    {
        PlayerController.player.inColorGame = true;
        UI.instance.PaintingProgress.gameObject.SetActive(true);
        colorGame.SetActive(true);
        playerController.characterSpawn = characterSpawn;
        playerController.roomCenter = roomCenter;
        playerController.Blink.SetTrigger("Active");
        playerController.TeleportType = 0;
    }
}
