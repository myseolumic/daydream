﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeController : MonoBehaviour
{

    public DoorPicker[] row1;
    public DoorPicker[] row2;
    public DoorPicker[] row3;
    public DoorPicker[] row4;
    public DoorPicker[] row5;
    public List<DoorPicker[]> rows;


    private void Start()
    {
        
 
    }

    public void ResetMaze()
    {
        List<DoorPicker[]> rows = new List<DoorPicker[]> { row1, row2, row3, row4, row5 };
        int random = Random.Range(0, 4);
        
        for (int i = 0; i < rows.Count; i++)
        {
            DoorPicker[] currentRow = rows[i];
            for (int j = 0; j < currentRow.Length ; j++)
            {
                currentRow[j].ResetDoors();
            }
        }

        DoorPicker first = rows[random][8];
        first.rightDoor.isCorrect = true;
    }

}
