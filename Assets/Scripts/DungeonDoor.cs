﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonDoor : MonoBehaviour, InteractableItem {

    public PlayerController playerController;

    public Transform characterSpawn;
    public Transform roomCenter;

    public GameObject legos;

    public void Interact()
    {
        playerController.characterSpawn = characterSpawn;
        playerController.roomCenter = roomCenter;
        playerController.TeleportType = 0;
        playerController.Blink.SetTrigger("Active");
        legos.GetComponent<CircleCollider2D>().enabled = true;
        
    }
}
