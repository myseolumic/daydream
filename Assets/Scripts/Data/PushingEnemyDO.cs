﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DayDream/Enemy/PushingEnemy")]
public class PushingEnemyDO : ScriptableObject
{
    // Use if we need hp
    public int startingHp;
    public int strength;
    public Sprite sprite;
    public float speed;
    public List<int> wave;
}
