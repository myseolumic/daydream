﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DayDream/Enemy/ShootingEnemy")]
public class ShootingEnemyDO : ScriptableObject {

    public int startingHp;
    public Projectile projectile;
    public Sprite sprite;
    public float speed;
    public float cooldown;
    public int dmg;
}
