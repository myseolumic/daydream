﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemy : MonoBehaviour {

    private float currentCooldown;
    public float cooldown;
    public int health;
    public int dmgAmmount;
    public float speed;
    public bool readyToShoot;
    public Turret t1;
    public Turret t2;
    public Turret t3;
    public Turret t4;

    // Use this for initialization
    void Start () {
        currentCooldown = 1;
    }
	
	// Update is called once per frame
	void Update () {
        if (readyToShoot)
        {
            t1.readyToShoot = true;
            t2.readyToShoot = true;
            t3.readyToShoot = true;
            t4.readyToShoot = true;
        }
        else
        {
            t1.readyToShoot = false;
            t2.readyToShoot = false;
            t3.readyToShoot = false;
            t4.readyToShoot = false;
        }

        if (currentCooldown > 0)
        {
            print("damage");
            currentCooldown -= Time.deltaTime;
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0.5f);
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        }
            

        if (health <= 0)
        {
 
            Destroy(this.gameObject);
            
        }
            
	}

    public void DealDamage()
    {
        if (currentCooldown >= 0 || health <= 0)
            return;

        health -= dmgAmmount;
        currentCooldown = cooldown;
    }

    
}
