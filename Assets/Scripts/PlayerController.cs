﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Audio related things
    public AudioClipGroup footstepClipGroup;
    public AudioClipGroup hitClipGroup;
    public AudioClipGroup levelVictorySound;
    public AudioClipGroup casualSound;

    //Movement & actions
    public GameObject attackRangeObject;
    public static PlayerController player;
    private Rigidbody2D rb;
    private bool inputDisabled;
    
    //knockback
    private bool gettingKnocked;
    private float knockBackTime = 0.5f;

    [Range(0, 10)]
    public float speed;


    //Status
    [HideInInspector]
    public int currentToyWave = 1;

    [Range(0, 5)]
    public int health = 5;

    [HideInInspector]
    public bool defeated = false;

    [HideInInspector]
    public bool beingThrown = false;
    
    [HideInInspector]
    public bool attacking = false;

    [HideInInspector]
    private bool inInterRange = false;

    private float attackingCounter = 0;

    //Objects
    private InteractableItem interactableItem;

    [HideInInspector]
    public Transform characterSpawn;

    [HideInInspector]
    public Transform roomCenter;

    [HideInInspector]
    public Animator Blink;

    [HideInInspector]
    public int TeleportType;

    public Transform defeatedSpawn;
    public Transform defeatedCameraPos;

    public ExclMark exclmark;
    public Animator animator;

    public bool fightingTeddy;
    public bool inColorGame;

    public Trap trapPrefab;
    public GameObject paintSpillPrefab;

    private float trapCooldown = 0;

    private float paintSpillChance = 0.1f;

    private float elapsed = 0f;

    private MazeDoor mazeDoor;
    private bool inMazeDoorRange = false;

    public BoxCollider2D normalCollider;
    public BoxCollider2D mazeCollider;

    public Transform mazeWrongChoiceSpawn;

    void Awake()
    {
        player = this;
        gettingKnocked = false;
        rb = GetComponent<Rigidbody2D>();
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    void Start()
    {
        Blink = UI.instance.gameObject.GetComponent<Animator>();
    }

    void Update()
    {

        casualSound.Play();

        //Handling for player death
        if (health <= 0)
        {
            defeated = true;
            UI.instance.LoseGame();          
            TeleportType = 1;
            DestroyAllObjects("ShowerMonster");
            DestroyAllObjects("EnemyProjectile");
            DestroyAllObjects("PushingEnemy");
            beingThrown = false;
            fightingTeddy = false;
            health = 5;
            Blink.SetTrigger("Active");

        }
        if (defeated) return;

        //Movement
        if (!gettingKnocked && !inputDisabled)
        {
            rb.velocity = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")) * speed;
            if (rb.velocity != Vector2.zero) footstepClipGroup.Play();
            animator.SetFloat("Horizontal", rb.velocity.x);
            animator.SetFloat("Vertical", rb.velocity.y);
            animator.SetFloat("Magnitude", rb.velocity.magnitude);
        }

        //Combat


        
        if (fightingTeddy)
        {
            if (trapCooldown > 0)
            {
                trapCooldown -= Time.deltaTime;
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (trapCooldown <= 0)
                {
                    trapCooldown = 5;
                    Instantiate(trapPrefab, transform.position, Quaternion.identity);
                }
            }
        }


        //Color game
        if (inColorGame) {

            if (UI.instance.PaintingProgress.fillAmount > 0.25f)
            {
                paintSpillChance = 0.2f;
            }
            else if (UI.instance.PaintingProgress.fillAmount > 0.50f)
            {
                paintSpillChance = 0.3f;
            }
            else if (UI.instance.PaintingProgress.fillAmount > 0.75f)
            {
                paintSpillChance = 0.4f;
            }
            else
            {
                paintSpillChance = 0.1f;
            }

            elapsed += Time.deltaTime;
            if (elapsed >= 1f)
            {
                elapsed = elapsed % 1f;
                SpillPaint(paintSpillChance);
            }
        }
        

        /*
        if (attackingCounter > 0)
        {
            attackingCounter -= 4 * Time.deltaTime;
        }
        else
        {
            attacking = false;
            attackRangeObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Space) && !attacking)
        {
            attacking = true;
            attackingCounter = 1f;
            attackRangeObject.SetActive(true);
            hitClipGroup.Play();
        }*/

        //Interactable objects
        if (inInterRange)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                interactableItem.Interact();
            }
        }

        if (inMazeDoorRange)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                mazeDoor.TryDoor();
            }
        }
       
    }

    void SpillPaint(float chance)
    {
        int random = Random.Range(1, 101);

        if (chance == 0.1f)
        {
            if(random <= 10)
            {
                Instantiate(paintSpillPrefab, transform.position, Quaternion.identity);
            }
        }

        if (chance == 0.2f)
        {
            if (random <= 20)
            {
                Instantiate(paintSpillPrefab, transform.position, Quaternion.identity);
            }
        }

        if (chance == 0.3f)
        {
            if (random <= 30)
            {
                Instantiate(paintSpillPrefab, transform.position, Quaternion.identity);
            }
        }

        if (chance == 0.4f)
        {
            if (random <= 40)
            {
                Instantiate(paintSpillPrefab, transform.position, Quaternion.identity);
            }
        }

        
    }


    //Collision handling
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "DroppedItem")
        {
            Debug.Log("Adding item to inv...");
            PickUppableItem item = collision.gameObject.GetComponent<PickUppableItem>();
            UI.instance.AddToInventory(item);
            Destroy(collision.gameObject);
        }

        if (collision.tag == "Interactable")
        {
            interactableItem = collision.gameObject.GetComponent<InteractableItem>();
            inInterRange = true;
            exclmark.gameObject.SetActive(true);

        }

        if(collision.tag == "EnemyProjectile")
        {
            print("hit");
            Damage(1);
            Destroy(collision.gameObject);
        }

        if (collision.tag == "Trap")
        {
            Damage(1);
            collision.GetComponent<Trap>().Trigger();
        }

        if (collision.tag == "Paint")
        {
            speed -= 4;
        }

        if (collision.tag == "MazeDoor")
        {
            inMazeDoorRange = true;
            mazeDoor = collision.GetComponent<MazeDoor>();
            //collision.GetComponent<MazeDoor>().TryDoor();
        }

        if (collision.tag == "DoorPicker")
        {
            collision.GetComponent<DoorPicker>().PickNextDoor();
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log(collision.tag);
        if (collision.tag == "Interactable")
        {
            exclmark.gameObject.SetActive(false);
            inInterRange = false;
        }

        if (collision.tag == "Paint")
        {
            speed += 4;
        }

    }

    //Misc. functions
    public void PlayVictorySound()
    {
        levelVictorySound.Play();
    }

    public void Teleport()
    {
        //Teleported to dungeon 1
        if (TeleportType == 0)
        {
            Vector3 nextPosition = characterSpawn.position;
            gameObject.transform.position = nextPosition;

            Vector3 cameraNextCenter = roomCenter.position;
            cameraNextCenter.z = -10;
            Camera.main.transform.position = cameraNextCenter;

            
        }

        //Defeated - teleported back to the first room
        else if (TeleportType == 1)
        {
            Vector3 nextPosition = defeatedSpawn.position;
            gameObject.transform.position = nextPosition;

            Vector3 cameraNextCenter = defeatedCameraPos.position;
            cameraNextCenter.z = -10;
            Camera.main.transform.position = cameraNextCenter;
        }


        //Ajutine
        UI.instance.winText.gameObject.SetActive(false);
        UI.instance.loseText.gameObject.SetActive(false);
        defeated = false;
        
    }

    public void Damage(int amount)
    {
        if (health <= 0) return;
        //flashing
        health -= amount;
        Debug.Log(health);
    }

    public void Damage(int amount, Vector2 direction)
    {
        Damage(amount);
        gettingKnocked = true;
        rb.AddForce(direction, ForceMode2D.Impulse); //borken
        StartCoroutine(knockBack());
        
    }

    private IEnumerator knockBack()
    {
        yield return new WaitForSeconds(knockBackTime);
        gettingKnocked = false;
    } 
        
    void DestroyAllObjects(string tag)
    {
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(tag);

        for (var i = 0; i < gameObjects.Length; i++)
        {
            Destroy(gameObjects[i]);
        }
    }

    public void SetInputEnabled(bool value)
    {
        if (!value)
        {
            inputDisabled = true;
        }
        else
        {
            inputDisabled = false;
        }
    }

    public void AnimateCurrentMovement()
    {
        animator.SetFloat("Horizontal", rb.velocity.x);
        animator.SetFloat("Vertical", rb.velocity.y);
        animator.SetFloat("Magnitude", rb.velocity.magnitude);
    }
}
