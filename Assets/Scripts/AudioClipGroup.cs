﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AudioClipGroup")]
public class AudioClipGroup : ScriptableObject {

    [Range(0,1)]
    public float VolumeMin = 1;
    [Range(0, 1)]
    public float VolumeMax = 1;
    [Range(-3, 3)]
    public float PitchMin = 1;
    [Range(-3, 3)]
    public float PitchMax = 1;

    public float Cooldown = 0.1f;
    public AudioClip[] audioClips;

    private float timeStamp;
    private AudioSourcePool audioSourcePool;

    private void OnEnable()
    {
        timeStamp = -Cooldown;
        audioSourcePool = FindObjectOfType<AudioSourcePool>();
    }

    public void Play()
    {
        if(!audioSourcePool) audioSourcePool = FindObjectOfType<AudioSourcePool>();

        AudioSource source = audioSourcePool.getSource();
        source.transform.SetParent(null);
        Play(source);
    }

    public void Play(Vector3 location)
    {
        if (!audioSourcePool) audioSourcePool = FindObjectOfType<AudioSourcePool>();

        AudioSource source = audioSourcePool.getSource();
        source.transform.position = location;
        source.transform.SetParent(null);
        
        Play(source);
    }

    public void Play(Transform transform)
    {
        if (!audioSourcePool) audioSourcePool = FindObjectOfType<AudioSourcePool>();

        AudioSource source = audioSourcePool.getSource();
        source.transform.SetParent(transform);
        source.transform.position = Vector3.zero;
        Play(source);
    }

    public void Play(AudioSource source)
    {
        if (audioClips.Length == 0) return;
        if (Time.time < timeStamp + Cooldown) return;
        timeStamp = Time.time;

        source.clip = audioClips[Random.Range(0, audioClips.Length)];
        source.volume = Random.Range(VolumeMin, VolumeMax);
        source.pitch = Random.Range(PitchMin, PitchMax);

        source.Play();
    }

    public void Stop()
    {
        audioSourcePool.StopAll();
    }
}
