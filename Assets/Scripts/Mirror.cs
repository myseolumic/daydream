﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mirror : MonoBehaviour, InteractableItem {

    public PlayerController playerController;
    //public ShootingEnemyDO shootingEnemyData;
    public ShootingEnemy shootingEnemy;
    public Transform characterSpawn;
    public Transform roomCenter;

    public void Interact()
    {
        StartCoroutine("SpawnMonster");

        playerController.characterSpawn = characterSpawn;
        playerController.roomCenter = roomCenter;
        playerController.Blink.SetTrigger("Active");
        playerController.TeleportType = 0;
    }

    IEnumerator SpawnMonster()
    {
        yield return new WaitForSeconds(3);
        ShootingEnemy showerMonster = Instantiate(shootingEnemy, new Vector2(-23.56f, 3.5f), Quaternion.identity);
        //showerMonster.sprite = shootingEnemyData.sprite;
        //showerMonster.cooldown = shootingEnemyData.cooldown;
        //showerMonster.projectile = shootingEnemyData.projectile;
        //showerMonster.dmgAmmount = shootingEnemyData.dmg;
        //showerMonster.speed = shootingEnemyData.speed;
        //showerMonster.health = shootingEnemyData.startingHp;
    }
}
